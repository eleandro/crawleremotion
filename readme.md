# Sistema crawleremotion
Siga as Instruções para instalação e teste do sistema.

## Instruções backend : devem ser executadas de dentro do diretório backend
1. Após clonar o repositório do sistema execute o comando abaixo para instalar as dependências:
`composer install`

2. Construa o banco de com o nome applicationtodo abra o arquivo .env.example, na raiz da pasta do sistema
 e configure as opções de DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME e DB_PASSWORD conforme a configuração do servidor. Renomeie o arquivo .env.example para .env.

3. Apartir da pasta raiz do sistema execute o comando abaixo para criar as estruturas de banco dados para o funcionamento eficaz do sistema:

`php artisan migrate:fresh`

`php artisan db:seed`

4. Os endpoints para testar o sistema são:

 GET `http://your_host_or_ip_system/importuniversitycourse`

 Importa informaçoes de cursos que foram crawleadas

 GET `http://your_host_or_ip_system/universitycourseregion`

 Mostra as notas de corte mínimas, média e máxima do top 5 cursos

 GET `http://your_host_or_ip_system/api/universitycoursemonthlypayment?state=MG&course=Agronomia&region=Suldeste`

 Mostra as mensalidades mínimas e máxima dos cursos conforme os filtros de estado, curso e região

## Instruções frontend : devem ser executadas de dentro do diretório frontend

1. Após clonar o repositório do sistema execute o comando abaixo para instalar as dependências:
`npm install`
2. Execute o sistema com o comando
`npm start`
3. Entre no sistema acessando:
`http://localhost:3000/`