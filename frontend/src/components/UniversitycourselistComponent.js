import React, { Component } from 'react' 
import { Container, Row, Col } from 'reactstrap' 
import SpacecraftlistinformationComponent from './spacecraft/UniversitycourseregionComponent' 
import UniversitycoursemonthlypaymentComponent from './spacecraft/UniversitycoursemonthlypaymentComponent' 

class UniversitycourselistComponent extends Component{ 
    
    render(){ 
        
        return ( 
            <Container> 
                <Row> 
                    <Col xs = "12" sm = "12" md = "12" className = "margin1em" > 
                        <SpacecraftlistinformationComponent /> 
                        
                    </Col> 
                    
                </Row>
                <Row> 
                     
                    <Col xs = "12" sm = "12" md = "12" className = "margin1em" > 
                    <UniversitycoursemonthlypaymentComponent /> 
                    </Col> 
                </Row> 
            </Container> 
        )
    }
} 

export default UniversitycourselistComponent
