import React, { Component } from 'react' 
import { Navbar, NavbarToggler, NavbarBrand, Collapse } from 'reactstrap' 

class HeaderComponent extends Component{ 

    constructor(props){ 
        
        super(props) 

        this.state = { isOpen : true }

        this.toggleNavbar = this
        .toggleNavbar
        .bind(this) 
    
    } 
    
    toggleNavbar(){ 
        this.setState({ 
            isOpen : !this.state.isOpen
        })
    } 
    
    render(){ 
        let isOpen = this.state.isOpen
        return ( 
            <header> 
                <Navbar color = "faded" light expand className = "navbarcustom" > 
                    <NavbarToggler right = "true" onClick = {this.toggleNavbar} /> 
                    <NavbarBrand href="/" >Dashboard</NavbarBrand> 
                    <Collapse isOpen = {isOpen} navbar = { true } > 
                    

                    </Collapse>
                </Navbar>
            </header>
        ) 

    }

} 

export default HeaderComponent