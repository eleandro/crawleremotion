import axios from 'axios' 

const ENTRYPOINTSERVER = 'http://localhost' 

const findcourse = () => { 
    
    const endpointrequest = `${ENTRYPOINTSERVER}/api/universitycourseregion` 
    
    return axios.get(endpointrequest).then(response => response)
} 
const finduniversitycoursemonthlypayment = (params) => { 
    
    const endpointrequest = `${ENTRYPOINTSERVER}/api/universitycoursemonthlypayment` 
    
    return axios.get(endpointrequest, {
        params: params
        } ).then( result => result ) 
    
}

export { 
    
    findcourse, 
    
    finduniversitycoursemonthlypayment 

} 
