import React, { Component } from 'react' 
import { Container, Row, Col, Form, FormGroup, Input, Button, Label, Table } from 'reactstrap' 
import { finduniversitycoursemonthlypayment } from './Universitycoursebridgebackend' 

class UniversitycoursemonthlypaymentComponent extends Component{ 
    
    constructor(props){ 
        
        super(props)
        
        this.state = { region : '', state : '', course : '', listcourse : [] } 
        
        this.handleChangeRegion = this.handleChangeRegion.bind(this) 

        this.handleChangeState = this.handleChangeState.bind(this) 

        this.handleChangeCourse = this.handleChangeCourse.bind(this) 
        
        this.handleSubmit = this.handleSubmit.bind(this) 
        
    } 
    
    handleChangeRegion(event){ 
        
        this.setState({ region : event.target.value}) 
    
    } 
    handleChangeState(event){ 
        
        this.setState({ state : event.target.value}) 
    
    } 
    handleChangeCourse(event){ 
        
        this.setState({ course : event.target.value}) 
    
    } 
    handleSubmit(event){ 
        
        let  state  = this.state.state
        let  region  = this.state.region
        let  course  = this.state.course
        let  params  = { region : region, state : state, course : course}
        console.log (params) 
        
        finduniversitycoursemonthlypayment(params).then(listcourse => this.setState( { listcourse : listcourse.data.data } ) ) 
    
    }
    
    componentDidMount(){ 
        let  params  = { region : '', state : '', course : ''}
        finduniversitycoursemonthlypayment(params).then(listcourse => this.setState( { listcourse : listcourse.data.data } ) )
        // findcourse().then(listcourse => console.log(listcourse) )
    }
    
    render(){ 
        
        let { listcourse } = this.state 

        return ( 
            <Container> 
                <Row> 
                    <Col xs = "12" sm = "12" md = "12" className = "margin1em" > 
                    <h2>Cursos e mensalidade</h2> 
                    </Col> 
                </Row>
                <Form> 
                    <Row> 
                        <Col xs = "8" sm = "8" md = "8"  > 
                            <FormGroup> 
                                <Label> Região: </Label>
                                <Input type="select" name="region" className = "form-control" id="region" onChange = { this.handleChangeRegion } >
                                    <option></option>
                                    <option>Norte</option>
                                    <option>Nordeste</option>
                                    <option>Centro-Oeste</option>
                                    <option>Suldeste</option>
                                    <option>Sul</option>
                                </Input> 
                                
                            </FormGroup> 
                        </Col> 
                        <Col xs = "4" sm = "4" md = "4" > 
                            <FormGroup> 
                            <Label> UF: </Label>
                                <Input type="select" name="state" className = "form-control" id="state" onChange = { this.handleChangeState } >
                                    
                                    <option></option>
                                    <option>AP</option>
                                    <option>AC</option>
                                    <option>AM</option>
                                    <option>RO</option>
                                    <option>PA</option>
                                    <option>TO</option>
                                    <option>RR</option>
                                    <option>AL</option>
                                    <option>BA</option>
                                    <option>CE</option>
                                    <option>MA</option>
                                    <option>PB</option>
                                    <option>PE</option>
                                    <option>PI</option>
                                    <option>RN</option>
                                    <option>SE</option>
                                    <option>DF</option>
                                    <option>GO</option>
                                    <option>MT</option>
                                    <option>MS</option>
                                    <option>ES</option>
                                    <option>MG</option>
                                    <option>RJ</option>
                                    <option>SP</option>
                                    <option>PR</option>
                                    <option>RS</option>
                                    <option>SC</option>
                                </Input> 
                            </FormGroup> 
                        </Col> 
                    </Row> 
                    <Row> 
                        <Col xs = "8" sm = "8" md = "8" className = "margin1em" > 
                            <FormGroup> 
                                <Label> Curso: </Label> 
                                <Input name = "course" type = "text" className = "form-control" id="course" onChange = { this.handleChangeCourse } /> 
                            </FormGroup> 
                        </Col> 
                        <Col xs = "4" sm = "4" md = "4" > 
                            <FormGroup> 
                                <Button color = "primary" className = "margintopbutton" onClick = { this.handleSubmit } > Filtrar </Button> 
                            </FormGroup> 
                        </Col> 
                    </Row> 
                </Form> 
                <Row> 
                    <Col xs = "12" sm = "12" md = "12" > 
                        <Table> 
                            <thead> 
                                <tr> 
                                    <th>Curso</th> 
                                    <th>Universidade</th> 
                                    <th>Estado</th> 
                                    <th>Região</th> 
                                    <th>Mensalidade Mínima</th> 
                                    <th>Mensalidade Máxima</th>
                                </tr>                                                 
                            </thead> 
                            <tbody> 
                            { listcourse.map((spacecraft, index) => ( 
                                <tr key = { index } > 
                                    <td>{spacecraft.course}</td> 
                                    <td>{spacecraft.university}</td> 
                                    <td>{spacecraft.uf}</td> 
                                    <td>{spacecraft.region}</td> 
                                    <td>{spacecraft.min}</td> 
                                    <td>{spacecraft.max}</td> 
                                </tr>  
                                ) ) } 
                            </tbody> 
                        </Table> 
                    </Col> 
                </Row> 
            </Container>
        )
    }
} 

export default UniversitycoursemonthlypaymentComponent
