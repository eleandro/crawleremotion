import React, { Component } from 'react' 
import { Container, Row, Col, Table } from 'reactstrap' 
import { findcourse } from './Universitycoursebridgebackend' 

class UniversitycourseregionComponent extends Component{ 
    
    constructor(props){ 
        
        super(props) 
        
        this.state = { listcourse : [] } 
        
    } 
    
    componentDidMount(){ 
        findcourse().then(listcourse => this.setState( { listcourse : listcourse.data.data } ) )
        // findcourse().then(listcourse => console.log(listcourse) )
    }
    
    render(){ 
        
        let { listcourse } = this.state 
        
        return ( 
            <Container> 
                <Row> 
                    <Col xs = "12" sm = "12" md = "12" className = "margin1em" > 
                    <h2>Top 5 cursos </h2> 
                    </Col> 
                </Row> 
                <Row> 
                    <Col xs = "12" sm = "12" md = "12" > 
                        <Table> 
                            <thead> 
                                <tr> 
                                    <th>Curso</th> 
                                    <th>Região</th> 
                                    <th>Mínima</th> 
                                    <th>Média</th> 
                                    <th>Máxima</th> 
                                </tr>                                                 
                            </thead> 
                            <tbody> 
                            { listcourse.map((course, index) => ( 
                                <tr key = { index } > 
                                    <td>{course.course}</td> 
                                    <td>{course.name}</td> 
                                    <td>{course.min}</td> 
                                    <td>{course.avg}</td> 
                                    <td>{course.max}</td> 
                                </tr>  
                                ) ) } 
                                                                               
                            </tbody> 
                        </Table> 
                    </Col> 
                </Row> 
            </Container> 
        )
    }
} 

export default UniversitycourseregionComponent
