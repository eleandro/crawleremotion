import React, { Component } from 'react' 
import 'bootstrap/dist/css/bootstrap.css' 
import './App.css' 
import { BrowserRouter as Rotas, Route } from 'react-router-dom' 
import HeaderComponent from './components/HeaderComponent' 
import UniversitycourselistComponent from './components/UniversitycourselistComponent' 
import UniversitycourseregionComponent from './components/spacecraft/UniversitycourseregionComponent' 
import UniversitycoursemonthlypaymentComponent from './components/spacecraft/UniversitycoursemonthlypaymentComponent' 

class App extends Component { 
  
  render() {
    return (
      <div className="App">
        <HeaderComponent/> 
        <Rotas> 
          
          <div> 
            
            <Route exact path = "/" component = {UniversitycourselistComponent} /> 
            <Route exact path = "/spacecraft" component = {UniversitycourseregionComponent} /> 
            <Route exact path = "/universitycoursemonthlypayment" component = {UniversitycoursemonthlypaymentComponent} /> 
            
          </div> 
          
        </Rotas>
      </div>
    );
  }
}

export default App;
