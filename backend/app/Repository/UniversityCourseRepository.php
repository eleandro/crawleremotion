<?php

namespace App\Repository;

use App\Model\UniversityCourse;
use Illuminate\Support\Facades\DB;

class UniversityCourseRepository
{
    private $universityCourseModel = null;

    public function __construct(UniversityCourse $universityCourseModel)
    {
        $this->universityCourseModel = $universityCourseModel;
    }

    public function getUniversityCourseModel()
    {
        return $this->universityCourseModel;
    }

    public function getInfoUniversityCourse($universityCourse)
    {
        return $this->universityCourseModel
            ->where('state_id', $universityCourse['state_id']) 
            ->where('city', $universityCourse['city']) 
            ->where('university', $universityCourse['university']) 
            ->where('university_campus', $universityCourse['university_campus']) 
            ->where('course', $universityCourse['course']) 
            ->where('degree', $universityCourse['degree']) 
            ->where('shift', $universityCourse['shift']) 
            ->where('monthly_payment', $universityCourse['monthly_payment']) 
            ->where('broad_integral_note', $universityCourse['broad_integral_note']) 
            ->first();
    }

    public function insertUniversityCourse($universityCourse)
    {
        $getUniversityCourseModel = $this->getInfoUniversityCourse($universityCourse);
        if (empty($getUniversityCourseModel)) {
            $this->universityCourseModel->create($universityCourse);
        }
    }

    public function getUniversityCoursePerRegion()
    {
        return DB::table('university_courses')
        ->select('university_courses.course', 'regions.name', 
        DB::Raw('min(university_courses.broad_integral_note) as min'), 
        DB::Raw('avg(university_courses.broad_integral_note) as avg'), 
        DB::Raw('max(university_courses.broad_integral_note) as max') 
        )
        ->join('states', 'university_courses.state_id', '=', 'states.id')
        ->join('regions', 'states.region_id', '=', 'regions.id')
        ->groupBy('regions.name', 'university_courses.course')
        ->limit(5)
        ->get();
    }

    public function getUniversityCoursePerMonthlyPayment($filter = [])
    {
        $query = DB::table('university_courses')
        ->select('university_courses.university', 
        'university_courses.course', 
        'states.uf', 
        'regions.name as region', 
        DB::Raw('min(university_courses.monthly_payment) as min'), 
        DB::Raw('max(university_courses.monthly_payment) as max') 
        )
        ->join('states', 'university_courses.state_id', '=', 'states.id')
        ->join('regions', 'states.region_id', '=', 'regions.id')
        ->groupBy('university_courses.university', 
        'university_courses.course', 
        'states.uf', 
        'regions.name');
        if (!empty($filter['course'])) {
            $query->where('course', $filter['course']) ;
        }

        if (!empty($filter['state'])) {
            $query->where('states.uf', $filter['state']) ;
        }

        if (!empty($filter['region'])) {
            $query->where('regions.name', $filter['region']) ;
        }
        return $query->get();
    }
}
