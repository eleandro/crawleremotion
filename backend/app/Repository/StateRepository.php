<?php

namespace App\Repository;

use App\Model\State;

class StateRepository
{
    private $stateModel = null;

    public function __construct(State $stateModel)
    {
        $this->stateModel = $stateModel;
    }

    public function getStateModel()
    {
        return $this->stateModel;
    }

    public function getUfIdPair()
    {
        $states = $this->stateModel->all();
        $idUfPair = [];
        foreach ($states as $state) {
            $idUfPair[$state['id']] = $state['uf'];
        }
        return array_flip($idUfPair);
    }
}
