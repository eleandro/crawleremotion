<?php

namespace App\Repository;

class UniversityCourseCrawlerRepository
{
    private $patternTotalUniversityCourse = '~(\s){1,}(([0-9]){1,})(\-){1,1}(([0-9]){1,})(\ de um total de ){1,1}(([0-9]|\.){1,})~';
    
    private $intervalPerPage = 0;
    
    private $maxTimesExecuteTracking = 0;
    
    private $endpointCrawler = 'https://brasil.io/dataset/cursos-prouni/cursos';

    public function setQuantityUniversityCourse($htmlUniversityCourse)
    {
        if ($this->intervalPerPage == 0) {
            if (preg_match($this->patternTotalUniversityCourse, $htmlUniversityCourse, $infoQuantityUniversityCourse)) {
                $this->intervalPerPage = $infoQuantityUniversityCourse[5] - ($infoQuantityUniversityCourse[3] - 1);
                $this->maxTimesExecuteTracking = ceil(str_replace('.', '', $infoQuantityUniversityCourse[8]) / $this->intervalPerPage);
            }
        }
    }
    
    public function getIntervalPerPage()
    {
        return $this->intervalPerPage;
    }

    public function getMaxTimesExecuteTracking()
    {
        return $this->maxTimesExecuteTracking;
    }
    
    public function getEndpointCrawler($page = 1)
    {
        if ($page != 1) {
            $endpointCrawler = $this->endpointCrawler;
            $endpointCrawler .= '?';
            $endpointCrawler .= http_build_query(['page' => $page]);
            return $endpointCrawler;
        } else {
            return $this->endpointCrawler;
        }
    }
}
