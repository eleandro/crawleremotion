<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\UniversityCourseCrawlerService;

class ImportuniversitycourseController extends Controller
{
    private $service = null;

    public function __construct(
        UniversityCourseCrawlerService $universityCourseCrawlerService
        )
    {
        $this->service = $universityCourseCrawlerService;
    }
    
    public function index(Request $request)
    {
        $this->service->importUniversityCourse(); 
    }
}
