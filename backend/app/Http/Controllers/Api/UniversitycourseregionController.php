<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\UniversityCourseRepository;

class UniversitycourseregionController extends Controller
{
    private $repository = null;

    public function __construct(
        UniversityCourseRepository $repository
        )
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $rows = $this->repository->getUniversityCoursePerRegion(); 

        if( !empty($rows)) {
            return response()->json(["data" => $rows, "message" => "Cursos obtidos com sucesso"], 200);
        } else {
            return response()->json(["data" => [], "message" => "Não foram obtidos cursos obtidos com sucesso"], 404);
        }
    }
}
