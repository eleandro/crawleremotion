<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\UniversityCourseRepository;

class UniversitycoursemonthlypaymentController extends Controller
{
    private $repository = null;

    public function __construct(
        UniversityCourseRepository $repository
        )
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $filter = $request->all();
        $rows = $this->repository->getUniversityCoursePerMonthlyPayment($filter); 

        if( !empty($rows)) {
            return response()->json(["data" => $rows, "message" => "Preço de mensalidade obtido com sucesso"], 200);
        } else {
            return response()->json(["data" => [], "message" => "Não foram obtidos preços de mensalidade, refine os filtros da sua busca"], 404);
        }
    }
}
