<?php

namespace App\Service;

use App\Repository\StateRepository;
use App\Repository\UniversityCourseRepository;
use App\Repository\UniversityCourseCrawlerRepository;
use Symfony\Component\DomCrawler\Crawler;

class UniversityCourseCrawlerService
{
    private $stateRepository = null;
    
    private $universityCourseRepository = null;
    
    private $universityCourseCrawlerRepository = null;
    
    public function __construct(
        StateRepository $stateRepository, 
        UniversityCourseRepository $universityCourseRepository, 
        UniversityCourseCrawlerRepository $universityCourseCrawlerRepository 
        )
    {
        $this->stateRepository = $stateRepository;
        
        $this->universityCourseRepository = $universityCourseRepository;
        
        $this->universityCourseCrawlerRepository = $universityCourseCrawlerRepository;
    }

    public function importUniversityCourse()
    {
        set_time_limit(0);
        $html = file_get_contents($this->universityCourseCrawlerRepository->getEndpointCrawler());
        $crawler = new Crawler($html);
        $paginationCrawler = $crawler->filter('.pagination'); 
        $this->setConfigCrawler($paginationCrawler);
        $crawler = $crawler->filter('#cursos-prouni > tbody > tr'); 
        $column = $this->getColumnUniversityCourseRepository();
        $ufIdPair = $this->stateRepository->getUfIdPair(); 
        $this->importUniversityCourseHtml($crawler, $column, $ufIdPair);
        $this->importUniversityCourseHtmlPage($crawler, $column);
    }

    public function importUniversityCourseHtml($crawler, $column, $ufIdPair)
    {
        if ($this->universityCourseCrawlerRepository->getIntervalPerPage()) {
            foreach ($crawler as $domElement) {
                $columnFound = $domElement->getElementsByTagName('td');
                $universityCourse = [];
                foreach ($column as $key => $columName) {
                    $element = $columnFound->item($key);
                    $universityCourse[$columName] = $element->nodeValue;
                }
                $universityCourse['state_id'] = $ufIdPair[$universityCourse['state_id']];
                $universityCourse = $this->treatmentUniversityCourseBeforeImport($column, $universityCourse);
                $this->universityCourseRepository->insertUniversityCourse($universityCourse);
            }
        }
    }

    public function treatmentUniversityCourseBeforeImport($column, $universityCourse)
    {
        foreach ($column as $key => $columName) {
            if (empty($universityCourse[$columName])) {
                $universityCourse[$columName] = null;
            }
        }
        $universityCourse['monthly_payment'] = floatval($universityCourse['monthly_payment']);
        $universityCourse['broad_integral_note'] = floatval($universityCourse['broad_integral_note']);
        return $universityCourse;
    }

    public function importUniversityCourseHtmlPage($column)
    {
        $ufIdPair = $this->stateRepository->getUfIdPair(); 
        for ($currentPage = 2; $currentPage <= $this->universityCourseCrawlerRepository->getMaxTimesExecuteTracking(); $currentPage++) {
            // sleep(5);
            $html = file_get_contents($this->universityCourseCrawlerRepository->getEndpointCrawler($currentPage));
            $crawler = new Crawler($html);
            $crawler = $crawler->filter('#cursos-prouni > tbody > tr'); 
            $column = $this->getColumnUniversityCourseRepository();
            $this->importUniversityCourseHtml($crawler, $column, $ufIdPair);
        }
    }

    public function setConfigCrawler($paginationCrawler)
    {
        foreach ($paginationCrawler as $domElement) {
            $itemListFound = $domElement->getElementsByTagName('li');
            $content = $itemListFound->item(0)->nodeValue;
            $this->universityCourseCrawlerRepository->setQuantityUniversityCourse($content);
            break;
        }
    }

    public function getColumnUniversityCourseRepository()
    {
        return [
            0 => 'state_id', 
            1 => 'city', 
            2 => 'university', 
            3 => 'university_campus', 
            4 => 'course', 
            5 => 'degree', 
            6 => 'shift', 
            7 => 'monthly_payment', 
            12 => 'broad_integral_note', 
        ];
    }
}
