<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';

    protected $fillable = [
        'name', 
    ];

    public function states()
    {
        return $this->hasMany('App\Model\State');
    }
}
