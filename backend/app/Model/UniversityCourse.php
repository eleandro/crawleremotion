<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UniversityCourse extends Model
{
    protected $table = 'university_courses';

    protected $fillable = [
        'state_id', 'city', 'university', 'university_campus', 'course', 'degree', 'shift', 'monthly_payment', 'broad_integral_note', 
    ];

    public function state()
    {
        return $this->belongsTo('App\Model\State');
    }
}
