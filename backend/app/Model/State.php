<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $fillable = [
        'name', 'uf', 
    ];

    public function region()
    {
        return $this->belongsTo('App\Model\Region');
    }

    public function universityCourses()
    {
        return $this->hasMany('App\Model\UniversityCourse');
    }
}
