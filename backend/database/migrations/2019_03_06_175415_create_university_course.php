<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityCourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_courses', function(Blueprint $table){ 
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->foreign('state_id')->references('id')->on('states'); 
            $table->string('city', 250);
            $table->string('university', 250);
            $table->string('university_campus', 250);
            $table->string('course', 250);
            $table->string('degree', 50);
            $table->string('shift', 50);
            $table->decimal('monthly_payment', 18, 2);
            $table->decimal('broad_integral_note', 18, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_courses');
    }
}
