<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["name" => "Norte"],
            ["name" => "Nordeste"],
            ["name" => "Centro-Oeste"],
            ["name" => "Suldeste"],
            ["name" => "Sul"]
        ];

        Db::table('regions')->insert($data);
    }
}
