<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["region_id" => 1, "name" => "Acre", "uf" => "AC"],
            ["region_id" => 1, "name" => "Amapá", "uf" => "AP"],
            ["region_id" => 1, "name" => "Amazonas", "uf" => "AM"],
            ["region_id" => 1, "name" => "Pará", "uf" => "PA"],
            ["region_id" => 1, "name" => "Rondônia", "uf" => "RO"],
            ["region_id" => 1, "name" => "Roraima", "uf" => "RR"],
            ["region_id" => 1, "name" => "Tocantins", "uf" => "TO"],
            ["region_id" => 2, "name" => "Alagoas", "uf" => "AL"],
            ["region_id" => 2, "name" => "Bahia", "uf" => "BA"],
            ["region_id" => 2, "name" => "Ceará", "uf" => "CE"],
            ["region_id" => 2, "name" => "Maranhão", "uf" => "MA"],
            ["region_id" => 2, "name" => "Paraíba", "uf" => "PB"],
            ["region_id" => 2, "name" => "Pernambuco", "uf" => "PE"],
            ["region_id" => 2, "name" => "Piauí", "uf" => "PI"],
            ["region_id" => 2, "name" => "Río Grande do Norte", "uf" => "RN"],
            ["region_id" => 2, "name" => "Sergipe", "uf" => "SE"],
            ["region_id" => 3, "name" => "Distrito Federal", "uf" => "DF"],
            ["region_id" => 3, "name" => "Goiás", "uf" => "GO"],
            ["region_id" => 3, "name" => "Mato Grosso", "uf" => "MT"],
            ["region_id" => 3, "name" => "Mato Grosso do Sul", "uf" => "MS"],
            ["region_id" => 4, "name" => "Espírito Santo", "uf" => "ES"],
            ["region_id" => 4, "name" => "Minas Gerais", "uf" => "MG"],
            ["region_id" => 4, "name" => "Rio de Janeiro", "uf" => "RJ"],
            ["region_id" => 4, "name" => "São Paulo", "uf" => "SP"],
            ["region_id" => 5, "name" => "Paraná", "uf" => "PR"],
            ["region_id" => 5, "name" => "Rio Grande do Sul", "uf" => "RS"],
            ["region_id" => 5, "name" => "Santa Catarina", "uf" => "SC"]
        ];

        Db::table('states')->insert($data);
    }
}
